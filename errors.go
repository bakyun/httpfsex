package httpfsex

import "errors"

var errNoOverlap = errors.New("invalid range: failed to overlap")
