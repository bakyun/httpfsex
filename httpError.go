package httpfsex

import (
	"fmt"
	"net/http"
)

type httpError struct {
	Code    int
	Message string
}

func newHTTPError(code int) error {
	return &httpError{code, http.StatusText(code)}
}

func newHTTPErrorEx(code int, message string) error {
	return &httpError{code, message}
}
func (e *httpError) Error() string {
	return fmt.Sprintf("%v: %v", e.Code, e.Message)
}
