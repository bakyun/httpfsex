package httpfsex

import (
	"errors"
	"fmt"
	"io"
	"mime"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"
)

// New extended http file server
func New(fs http.FileSystem, options *Options) http.Handler {
	if options == nil {
		options = DefaultOptions
	}

	return &httpfs{
		root:    fs,
		options: options,
	}
}

type httpfs struct {
	root    http.FileSystem
	options *Options
}

func (f *httpfs) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	upath := path.Clean(r.URL.Path)
	if !strings.HasPrefix(upath, "/") {
		upath = "/" + upath
		r.URL.Path = upath
	}

	// strip directory index if enabled
	if f.options.StripDirectryIndex {
		for _, di := range f.options.DirectoryIndex {
			if strings.HasSuffix(upath, "/"+di) {
				localRedirect(w, r, "./")
				return
			}
		}
	}

	file, err := f.root.Open(upath)
	if err != nil {
		f.error(w, r, err)
		return
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		f.error(w, r, err)
		return
	}

	if f.options.Redirect {
		// redirect to canonical path: / at end of directory url
		// r.URL.Path always begins with /
		url := r.URL.Path
		if stat.IsDir() {
			if url[len(url)-1] != '/' && f.options.SlashDirectories {
				localRedirect(w, r, path.Base(url)+"/")
				return
			} else if url[len(url)-1] == '/' && !f.options.SlashDirectories {
				localRedirect(w, r, path.Base(url)+"/")
				return
			}
		} else {
			if url[len(url)-1] == '/' {
				localRedirect(w, r, "../"+path.Base(url))
				return
			}
		}
	}

	// use contents of index.html for directory, if present
	if stat.IsDir() {
		base := strings.TrimSuffix(upath, "/") + "/"
		for _, di := range f.options.DirectoryIndex {
			newFile, err := f.root.Open(base + di)
			if err == nil {
				defer newFile.Close()
				newStat, err := newFile.Stat()
				if err == nil {
					upath = base + di
					stat = newStat
					file = newFile
					break
				}
			}
		}
	}

	if stat.IsDir() {
		if !f.options.ListDirectoryContents {
			f.error(w, r, newHTTPError(http.StatusForbidden))
			return
		}

		if checkIfModifiedSince(r, stat.ModTime()) == condFalse {
			writeNotModified(w)
			return
		}

		w.Header().Set("Last-Modified", stat.ModTime().UTC().Format(http.TimeFormat))
		if f.options.DirectoryLister != nil {
			f.options.DirectoryLister(w, r, file)
		} else {
			f.dirList(w, r, file)
		}
		return
	}

	f.serve(w, r, upath, file, stat)
}

func (f *httpfs) serve(w http.ResponseWriter, r *http.Request, upath string, file http.File, stat os.FileInfo) {
	t := stat.ModTime()
	if !t.IsZero() && t != time.Unix(0, 0) {
		w.Header().Set("Last-Modified", t.UTC().Format(http.TimeFormat))
	}

	done, rangeReq := checkPreconditions(w, r, t)
	if done {
		return
	}

	code := http.StatusOK
	// If Content-Type isn't set, use the file's extension to find it, but
	// if the Content-Type is unset explicitly, do not sniff the type.
	ctypes, haveType := w.Header()["Content-Type"]
	var ctype string
	if !haveType {
		ctype = mime.TypeByExtension(filepath.Ext(stat.Name()))
		if ctype == "" {
			ctype = "application/octet-stream"
		}
		w.Header().Set("Content-Type", ctype)
	} else if len(ctypes) > 0 {
		ctype = ctypes[0]
	}

	size := stat.Size()
	sendSize := size
	var sendContent io.Reader = file
	if size >= 0 {
		ranges, err := parseRange(rangeReq, size)
		if err != nil {
			if err == errNoOverlap {
				w.Header().Set("Content-Range", fmt.Sprintf("bytes */%d", size))
			}
			http.Error(w, err.Error(), http.StatusRequestedRangeNotSatisfiable)
			return
		}
		if sumRangesSize(ranges) > size {
			// The total number of bytes in all the ranges
			// is larger than the size of the file by
			// itself, so this is probably an attack, or a
			// dumb client. Ignore the range request.
			ranges = nil
		}
		switch {
		case len(ranges) == 1:
			// RFC 7233, Section 4.1:
			// "If a single part is being transferred, the server
			// generating the 206 response MUST generate a
			// Content-Range header field, describing what range
			// of the selected representation is enclosed, and a
			// payload consisting of the range.
			// ...
			// A server MUST NOT generate a multipart response to
			// a request for a single range, since a client that
			// does not request multiple parts might not support
			// multipart responses."
			ra := ranges[0]
			if _, err := file.Seek(ra.start, io.SeekStart); err != nil {
				http.Error(w, err.Error(), http.StatusRequestedRangeNotSatisfiable)
				return
			}
			sendSize = ra.length
			code = http.StatusPartialContent
			w.Header().Set("Content-Range", ra.contentRange(size))
		case len(ranges) > 1:
			sendSize = rangesMIMESize(ranges, ctype, size)
			code = http.StatusPartialContent

			pr, pw := io.Pipe()
			mw := multipart.NewWriter(pw)
			w.Header().Set("Content-Type", "multipart/byteranges; boundary="+mw.Boundary())
			sendContent = pr
			defer pr.Close() // cause writing goroutine to fail and exit if CopyN doesn't finish.
			go func() {
				for _, ra := range ranges {
					part, err := mw.CreatePart(ra.mimeHeader(ctype, size))
					if err != nil {
						pw.CloseWithError(err)
						return
					}
					if _, err := file.Seek(ra.start, io.SeekStart); err != nil {
						pw.CloseWithError(err)
						return
					}
					if _, err := io.CopyN(part, file, ra.length); err != nil {
						pw.CloseWithError(err)
						return
					}
				}
				mw.Close()
				pw.Close()
			}()
		}

		w.Header().Set("Accept-Ranges", "bytes")
		if w.Header().Get("Content-Encoding") == "" {
			w.Header().Set("Content-Length", strconv.FormatInt(sendSize, 10))
		}
	}

	w.WriteHeader(code)

	if r.Method != "HEAD" {
		io.CopyN(w, sendContent, sendSize)
	}
}

func (f *httpfs) error(w http.ResponseWriter, r *http.Request, err error) {
	if f.options.FallbackPath != "" && r.URL.Path != f.options.FallbackPath {
		file, err := f.root.Open(f.options.FallbackPath)
		if err == nil {
			defer file.Close()
			stat, err := file.Stat()
			if err == nil {
				f.serve(w, r, r.URL.Path, file, stat)
				return
			}
		}
	}

	herr := new(httpError)
	if errors.As(err, &herr) {
		http.Error(w, herr.Message, herr.Code)
		return
	}

	msg, code := toHTTPError(err)
	http.Error(w, msg, code)
}

func (f *httpfs) dirList(w http.ResponseWriter, r *http.Request, file http.File) {
	dirs, err := file.Readdir(-1)
	if err != nil {
		f.error(w, r, newHTTPErrorEx(http.StatusInternalServerError, "Error reading directory"))
		return
	}
	sort.Slice(dirs, func(i, j int) bool { return dirs[i].Name() < dirs[j].Name() })

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintf(w, "<pre>\n")
	for _, d := range dirs {
		name := d.Name()
		if d.IsDir() {
			name += "/"
		}

		url := url.URL{Path: name}
		fmt.Fprintf(w, "<a href=\"%s\">%s</a>\n", url.String(), htmlReplacer.Replace(name))
	}

	fmt.Fprintf(w, "</pre>\n")
}
