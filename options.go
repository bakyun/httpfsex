package httpfsex

import (
	"net/http"
)

// DefaultOptions for the file server
var DefaultOptions = &Options{
	Redirect:              true,
	FallbackPath:          "",
	DirectoryIndex:        []string{"index.html"},
	StripDirectryIndex:    true,
	SlashDirectories:      true,
	ListDirectoryContents: true,
}

// Options for the file sever
type Options struct {
	Redirect              bool
	FallbackPath          string
	DirectoryIndex        []string
	StripDirectryIndex    bool
	SlashDirectories      bool
	ListDirectoryContents bool
	DirectoryLister       func(http.ResponseWriter, *http.Request, http.File)
}
